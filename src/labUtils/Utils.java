package labUtils;

import java.util.ArrayList;

import treeClasses.LinkedBinaryTree;
import treeClasses.LinkedTree;
import treeInterfaces.Position;
import treeInterfaces.Tree;

public class Utils {
	public static <E> void displayTree(String msg, Tree<E> t) { 
		System.out.println("\n\n" + msg); 
		t.display(); 
	}

	public static <E> void displayTreeElements(String msg, Tree<E> t) {
		System.out.println("\n\n" + msg); 
		for (E element : t)
			System.out.println(element); 
		
	}
	
	public static LinkedTree<Integer> buildExampleTreeAsLinkedTree() { 
		LinkedTree<Integer> t = new LinkedTree<>(); 
		
		t.addRoot(4);
		
		Position<Integer> leaf1 = t.addChild(t.root(), 9);
		Position<Integer> leaf2 = t.addChild(t.root(), 20);
		
		t.addChild(leaf1, 7);
		t.addChild(leaf1, 10);
		
		Position<Integer> leaf21 = t.addChild(leaf2, 15);
		Position<Integer> leaf22 = t.addChild(leaf2, 21);
		
		t.addChild(leaf21, 12);
		Position<Integer> leaf212 = t.addChild(leaf21, 17);
		t.addChild(leaf212, 19);
		
		Position<Integer> leaf221 = t.addChild(leaf22, 40);
		t.addChild(leaf221, 30);
		t.addChild(leaf221, 45);
		
		return t; 
	}
	
	public static LinkedBinaryTree<Integer> buildExampleTreeAsLinkedBinaryTree() { 
		LinkedBinaryTree<Integer> t = new LinkedBinaryTree<>(); 
		
		t.addRoot(4);
		
		Position<Integer> leaf1 = t.addLeft(t.root(), 9);
		Position<Integer> leaf2 = t.addRight(t.root(), 20);
		
		t.addLeft(leaf1, 7);
		t.addRight(leaf1, 10);
		
		Position<Integer> leaf21 = t.addLeft(leaf2, 15);
		Position<Integer> leaf22 = t.addRight(leaf2,  21);
		
		t.addLeft(leaf21, 12);
		Position<Integer> leaf212 = t.addRight(leaf21, 17);
		Position<Integer> leaf221 = t.addRight(leaf22, 40);
		
		t.addLeft(leaf212, 19);
		t.addLeft(leaf221, 30);
		t.addRight(leaf221, 45);
		
		return t; 
	}


}
